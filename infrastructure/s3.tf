#################

# s3 resources

#################

resource "aws_s3_bucket" "tf_resource_s3_bucket" {
  bucket = "${local.prefix}"
  acl = "public-read"
  force_destroy = true

  policy = <<EOF
  {
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "PublicReadGetObject",
            "Action": [
                "s3:*"
            ],
            "Effect": "Allow",
            "Resource": "arn:aws:s3:::${local.prefix}/*",
            "Principal": "*"
            
        }
    ]

  }
  EOF

  versioning {
    enabled = true
  }
  
  tags = local.common_tags
}